How To Use The Amplifier

Each sensor should be connected to its own amplifier.

1. Connect the data pin of the sensors to the input of the amplifier.
    - To send the signal captured by the sensors.
2. Connect the output pin of the amplifier to the GPIO26 pin of the Raspberry pin
    - To send the amplified signal to the Pi
