## Bill of Material (BOM)

version: 01  
last update: 11/06/2021

| #| Part Number | Part Name | Quantity | Unit Price (ZAR) | Datasheets | Website/Documentation |
| ------ | ------ | ------ |------ |------ |------ |------ |
| 1 | Ra-07H | LoRaWAN™ module | 1 | - | [link](https://docs.ai-thinker.com/_media/lora/ra-07h_data_sheet_en.pdf) | [Ai-Thinker](https://docs.ai-thinker.com/en/lorawan) |
| 2 | LM35 | Temperature sensor | 1 | - | [link](https://www.ti.com/lit/ds/symlink/lm35.pdf?ts=1623438285227&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FLM35) | [Texas Instruments](https://www.ti.com/product/LM35?HQS=OCB-tistore-invf-partpage-invf-store-SnapEDA-wwe) |
| 3 | PDV-P8005 | Light sensor | 1 | - | [link](https://docs.rs-online.com/74c0/0900766b80cdbc6d.pdf) | [ Luna Optoelectronics ](WWW.LUNAINC.COM) |
| 4 | 1825910-6 | SPST Button Tactile Switch | 1 | - | [link](https://docs.rs-online.com/17fc/0900766b8160dc5b.pdf) | [ TE Connectivity ](https://za.rs-online.com/web/b/te-connectivity) |
| 5 | LS L29K-H1J2-1 | 1.8 V Red LED | 4 | - | [link](https://docs.rs-online.com/74c0/0900766b80cdbc6d.pdf) | [ OSRAM Opto Semiconductors ](https://za.rs-online.com/web/b/osram-opto-semiconductors) |
| 6 | CR0805 | Surface Mount Resistors | - | - | [link](https://www.bourns.com/docs/product-datasheets/CRxxxxx.pdf) | [ - ](https://za.rs-online.com) |
| 7 | LT1492 | Amplifier | 2 | - | [link](https://www.analog.com/media/en/technical-documentation/data-sheets/14923f.pdf) | [ - ](https://za.rs-online.com) |
| 8 | OKI-78SR-5/1.5-W36-C | Switching regulator | 1 | - | [link](https://docs.rs-online.com/9e15/0900766b813d7b66.pdf) | [ Murata Power Solutions ](https://za.rs-online.com/web/b/murata-power-solutions) |
| 9 | EEEFK1H221P | Capacitor | 1 | - | [link](https://docs.rs-online.com/919b/0900766b8139e2ba.pdf) | [ Panasonic ](https://za.rs-online.com/web/b/panasonic) |

Note: The Bill of Material here might slitly differ from the one used for the KiCAD PCB, do your own research on the components you wish to use  
Link to BOM (KiCAD PCB version) : [Link](https://gitlab.com/group-047/pihat-project/-/blob/main/kicad/remote_sensors_uHAT/remote_sensors_uHAT.csv)
