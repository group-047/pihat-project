## How to Get Started

**What you'll need**
1. A Raspberry Pi Zero (any version) (This should come with GPIO Headers or buy them yourself and solder them)
2. The Raspberry Pi LoRa-Sensor uHAT (Can also get screws but they not required for this work)
3. A device that can the Pi can exchange with via the LoRa network (this could be another HAT connected to a Raspberry Pi)
4. A small wire Antenna for both the recieveing and transmiting devices (Any form of antenna should work but do research before picking an antenna and check if it's compatable with the Ra-07H LoRa module)
5. A power supply source to the HAT (USB adaptor or 8-12 volts battery should be sufficient) or Raspberry Pi Power Supply if you want to supply directly to the Raspberry Pi
6. MicroSD card (with Raspbian)



Step 1: Putting things together


1. Plug the Raspberry Pi LoRa-Sensor uHAT into the Raspberry Pi Zero according to the GPIO PINs (make sure you don't mismatch the PINs) do this for both devices if you using the two Raspberry Pis and two HATs
2. 
