## What You'll Need

1. 1x Ra-07H LoRa Module 
2. 4x Red LED
3. 8x Resistors 
4. 1x Capacitor
5. 1x Switching Voltage Regulator IC
6. 1x MOSFET
7. 1x Inductor
8. 1x Rectifier Diode
7. 2x OpAmp
8. 1x Temparature Sensor
9. 1x SPST Switch

All the details for each of the components are available in the Bill of Material [ here ](https://gitlab.com/group-047/pihat-project/-/blob/main/BOM.md)


## The System

The system is divided into the following 3 subsystems;  
1. The Power Supply 
2. The Amplifier
3. The Status LEDs + Power Switch

In addition to the subsystems you'll also need the following
1. A LoRa module
2. Sensors 
3. A power supply source (You won't need to worry about this if you are going to power the Raspberry by plugging into a USB power supply)


## Step 1: Circuit Design

The first step is desigining the circuit in each subsystem, make sure it meets the specifications by testing and simulating it and once you are done you can go to the next step which is building the PCB version. 

The Power Supply  
The power supply will be supplying a voltage of 5 volts to the Raspberry Pi (It's also possible to power other sensors straight from the power supply but for this design the Raspberry Pi will be the one supplying a voltage to the other subsystem, the Pi can supply a voltage of 3.3 volts @ 50mA using PIN 1 or PIN 17 or 5 volts @ 1.5A using PIN 2 or PIN 5)

The power supply circuit is a simple switching voltage regulator available [here](https://gitlab.com/group-047/pihat-project/-/blob/main/ltspice/Power_Supply_Unit.asc)

This will take in a 12 volts unregulated voltage and step it down to a 5 volts regulated power supply (Other altenatives may be used but must be tested to meet the specifications) for the circuit you'll need the following components
1. MOSFET
2. Rectifier Diode
3. Inductor
4. Capacitor
5. Resistors

All values are available in the BOM (You may choose to use different value or components but makue they meet the specifications)
When you are done choosing the components and values you can test the circuit using a simulation tool for this project LTSpice was used.

The Status LED + Power Switch  

The status LEDs aew used to indicate the following 4 parameters
1. The AUX which show the working status of the LoRa module
2. Tx is a transmission line this will indicate when the LoRa is transmitting data to the Raspberry Pi
3. Rx is a recieving line for when the LoRa is recieving data from the Raspberry Pi
4. The last LED show the OB/OFF power status of Raspberry Pi 

The connection is simple and cane found [here](https://gitlab.com/group-047/pihat-project/-/blob/main/ltspice/Power_Supply_Unit.asc) 

The Amplifier  

-

The Sensors and LoRa Module  
For this you can pick any sensor you want use in this project a temperature sensor was used. The LoRa module used is the Ra-07H this was chosen because of it's small size to save space on the HAT. For this project no simulation was done for the sensors but if possible do test the components you choose to use.

## Step 2: Building a PCB Model 

After testing all the substystem is now time to build a PCB model for manufacturing, you can find PCB files here KiCAD was used to produce this PCB, you can use other available tools 

You need to produce the following files for printing your PCB
1. BOM (Schematic View)
2. PDF of Docs (Anywhere)
3. Pick and Place Files (PCB View)
4. Paste file(s) for Stencil (PCB View)



