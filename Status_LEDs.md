The are four status LEDs and functions as follows: 

1. LED 1 - Indicates when a voltage supply is connected to the Raspberry Pi (RPi), it’s connected to PIN 1 of the RPi which has an output of 3.3 volts when the RPi  is turned on. 
2. LED 2 - Indicates the LoraWAN module’s working status. It works the same as the LED 1 circuit but instead of being connected to ground it connects to the AUX pin on the LoraWAN module.
3. LED 3 - Indicates when the LoraWAN module is transmitting data to the RPi.  It works the same as the LED 1 circuit but instead of being connected to ground it connects to the Tx pin on the LoraWAN module.
4. LED 4 - Indicates when the LoraWAN module is receiving data from the RPi.  It works the same as the LED 1 circuit but instead of being connected to ground it connects to the Rx pin on the LoraWAN module.
