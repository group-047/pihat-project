EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Amplifier"
Date "2021-06-04"
Rev "1"
Comp "Group 47"
Comment1 "Author: Sabelo"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0401
U 1 1 60B9C0ED
P 4650 4350
F 0 "#PWR0401" H 4650 4100 50  0001 C CNN
F 1 "GND" H 4655 4177 50  0000 C CNN
F 2 "" H 4650 4350 50  0001 C CNN
F 3 "" H 4650 4350 50  0001 C CNN
	1    4650 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0404
U 1 1 60B9C8B9
P 6500 4350
F 0 "#PWR0404" H 6500 4100 50  0001 C CNN
F 1 "GND" H 6505 4177 50  0000 C CNN
F 2 "" H 6500 4350 50  0001 C CNN
F 3 "" H 6500 4350 50  0001 C CNN
	1    6500 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 4350 4650 3750
Wire Wire Line
	4650 3750 4800 3750
Wire Wire Line
	6750 3700 6500 3700
Wire Wire Line
	6500 3700 6500 4350
Wire Wire Line
	4450 3150 4650 3150
Wire Wire Line
	5900 3150 5700 3150
Wire Wire Line
	6600 3150 6700 3150
Wire Wire Line
	6700 3150 6700 3500
Wire Wire Line
	6700 3500 6750 3500
Wire Wire Line
	4800 3550 4650 3550
Wire Wire Line
	4650 3550 4650 3150
Wire Wire Line
	7750 3150 7600 3150
Connection ~ 6700 3150
Wire Wire Line
	5700 3150 5700 3650
Wire Wire Line
	5700 3650 5400 3650
Connection ~ 5700 3150
Wire Wire Line
	5700 3150 5500 3150
Wire Wire Line
	7350 3600 7600 3600
Wire Wire Line
	7600 3600 7600 3150
Connection ~ 7600 3150
Wire Wire Line
	7600 3150 6700 3150
$Comp
L Amplifier_Operational:LT1492 U401
U 3 1 60BA12C8
P 5100 3650
F 0 "U401" H 5058 3696 50  0001 L CNN
F 1 "LT1492" H 5250 3350 50  0001 L CNN
F 2 "lib:SOIC127P599X175-8N" H 5100 3650 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/14923f.pdf" H 5100 3650 50  0001 C CNN
	3    5100 3650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LT1492 U402
U 3 1 60BA527E
P 7050 3600
F 0 "U402" H 7008 3646 50  0001 L CNN
F 1 "LT1492" H 7008 3600 50  0001 L CNN
F 2 "lib:SOIC127P599X175-8N" H 7050 3600 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/14923f.pdf" H 7050 3600 50  0001 C CNN
	3    7050 3600
	1    0    0    -1  
$EndComp
Text HLabel 3750 3150 0    50   Input ~ 0
Vin
Text HLabel 8450 3150 2    50   Output ~ 0
Vout
$Comp
L power:+10V #PWR0402
U 1 1 60BA97EC
P 5000 3350
F 0 "#PWR0402" H 5000 3200 50  0001 C CNN
F 1 "+10V" H 4850 3450 50  0000 C CNN
F 2 "" H 5000 3350 50  0001 C CNN
F 3 "" H 5000 3350 50  0001 C CNN
	1    5000 3350
	1    0    0    -1  
$EndComp
$Comp
L power:-10V #PWR0403
U 1 1 60BAA268
P 5000 3950
F 0 "#PWR0403" H 5000 4050 50  0001 C CNN
F 1 "-10V" H 5015 4123 50  0000 C CNN
F 2 "" H 5000 3950 50  0001 C CNN
F 3 "" H 5000 3950 50  0001 C CNN
	1    5000 3950
	-1   0    0    1   
$EndComp
$Comp
L power:+10V #PWR0405
U 1 1 60BAB0EF
P 6950 3300
F 0 "#PWR0405" H 6950 3150 50  0001 C CNN
F 1 "+10V" H 7050 3350 50  0000 C CNN
F 2 "" H 6950 3300 50  0001 C CNN
F 3 "" H 6950 3300 50  0001 C CNN
	1    6950 3300
	1    0    0    -1  
$EndComp
$Comp
L power:-10V #PWR0406
U 1 1 60BAB9A4
P 6950 3900
F 0 "#PWR0406" H 6950 4000 50  0001 C CNN
F 1 "-10V" H 6965 4073 50  0000 C CNN
F 2 "" H 6950 3900 50  0001 C CNN
F 3 "" H 6950 3900 50  0001 C CNN
	1    6950 3900
	-1   0    0    1   
$EndComp
$Comp
L CRCW04021K00FKEDHP:CRCW04021K00FKEDHP R401
U 1 1 60C4542C
P 3750 3150
F 0 "R401" H 4100 3375 50  0000 C CNN
F 1 "CRCW04021K00FKEDHP" H 4100 3284 50  0000 C CNN
F 2 "lib:RESC1005X40N" H 4300 3200 50  0001 L CNN
F 3 "http://www.vishay.com/docs/20035/dcrcwe3.pdf" H 4300 3100 50  0001 L CNN
F 4 "CRCW Power Resistor 0402 0.063W 1K Vishay CRCW Series Thick Film Power Resistor 0402 Case 1k +/-1% 0.125W +/-100ppm/K" H 4300 3000 50  0001 L CNN "Description"
F 5 "0.4" H 4300 2900 50  0001 L CNN "Height"
F 6 "Vishay" H 4300 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDHP" H 4300 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDH" H 4300 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDHP?qs=FlFvgi4rxBxFJ5cCIk6dAw%3D%3D" H 4300 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDHP" H 4300 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedhp/vishay" H 4300 2300 50  0001 L CNN "Arrow Price/Stock"
	1    3750 3150
	1    0    0    -1  
$EndComp
$Comp
L CRCW04021K00FKEDHP:CRCW04021K00FKEDHP R403
U 1 1 60C46ABB
P 5900 3150
F 0 "R403" H 6250 3375 50  0000 C CNN
F 1 "CRCW04021K00FKEDHP" H 6250 3284 50  0000 C CNN
F 2 "lib:RESC1005X40N" H 6450 3200 50  0001 L CNN
F 3 "http://www.vishay.com/docs/20035/dcrcwe3.pdf" H 6450 3100 50  0001 L CNN
F 4 "CRCW Power Resistor 0402 0.063W 1K Vishay CRCW Series Thick Film Power Resistor 0402 Case 1k +/-1% 0.125W +/-100ppm/K" H 6450 3000 50  0001 L CNN "Description"
F 5 "0.4" H 6450 2900 50  0001 L CNN "Height"
F 6 "Vishay" H 6450 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDHP" H 6450 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDH" H 6450 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDHP?qs=FlFvgi4rxBxFJ5cCIk6dAw%3D%3D" H 6450 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDHP" H 6450 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedhp/vishay" H 6450 2300 50  0001 L CNN "Arrow Price/Stock"
	1    5900 3150
	1    0    0    -1  
$EndComp
$Comp
L CRCW04021K00FKEDHP:CRCW04021K00FKEDHP R404
U 1 1 60C47F67
P 7750 3150
F 0 "R404" H 8100 3375 50  0000 C CNN
F 1 "CRCW04021K00FKEDHP" H 8100 3284 50  0000 C CNN
F 2 "lib:RESC1005X40N" H 8300 3200 50  0001 L CNN
F 3 "http://www.vishay.com/docs/20035/dcrcwe3.pdf" H 8300 3100 50  0001 L CNN
F 4 "CRCW Power Resistor 0402 0.063W 1K Vishay CRCW Series Thick Film Power Resistor 0402 Case 1k +/-1% 0.125W +/-100ppm/K" H 8300 3000 50  0001 L CNN "Description"
F 5 "0.4" H 8300 2900 50  0001 L CNN "Height"
F 6 "Vishay" H 8300 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "CRCW04021K00FKEDHP" H 8300 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "71-CRCW04021K00FKEDH" H 8300 2600 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Vishay-Dale/CRCW04021K00FKEDHP?qs=FlFvgi4rxBxFJ5cCIk6dAw%3D%3D" H 8300 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "CRCW04021K00FKEDHP" H 8300 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/crcw04021k00fkedhp/vishay" H 8300 2300 50  0001 L CNN "Arrow Price/Stock"
	1    7750 3150
	1    0    0    -1  
$EndComp
$Comp
L CR0402-FX-6800GLF:CR0402-FX-6800GLF R402
U 1 1 60C53062
P 4800 3150
F 0 "R402" H 5150 3375 50  0000 C CNN
F 1 "CR0402-FX-6800GLF" H 5150 3284 50  0000 C CNN
F 2 "lib:RESC1005X40N" H 5350 3200 50  0001 L CNN
F 3 "https://www.bourns.com/pdfs/CR0402.pdf" H 5350 3100 50  0001 L CNN
F 4 "Thick Film Resistors - SMD 680 OHM 1%" H 5350 3000 50  0001 L CNN "Description"
F 5 "0.4" H 5350 2900 50  0001 L CNN "Height"
F 6 "Bourns" H 5350 2800 50  0001 L CNN "Manufacturer_Name"
F 7 "CR0402-FX-6800GLF" H 5350 2700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CR0402FX-6800GLF" H 5350 2600 50  0001 L CNN "Mouser Part Number"
F 9 "http://www.mouser.com/Search/ProductDetail.aspx?qs=ePR1ZvdkOKKH2x0bUeY3fg%3d%3d" H 5350 2500 50  0001 L CNN "Mouser Price/Stock"
F 10 "CR0402-FX-6800GLF" H 5350 2400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cr0402-fx-6800glf/bourns" H 5350 2300 50  0001 L CNN "Arrow Price/Stock"
	1    4800 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3150 4650 3150
Connection ~ 4650 3150
$Comp
L power:PWR_FLAG #FLG0401
U 1 1 60C72301
P 5250 3350
F 0 "#FLG0401" H 5250 3425 50  0001 C CNN
F 1 "PWR_FLAG" H 5250 3523 50  0000 C CNN
F 2 "" H 5250 3350 50  0001 C CNN
F 3 "~" H 5250 3350 50  0001 C CNN
	1    5250 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3350 5000 3350
Connection ~ 5000 3350
$Comp
L power:PWR_FLAG #FLG0402
U 1 1 60C747DA
P 5250 3950
F 0 "#FLG0402" H 5250 4025 50  0001 C CNN
F 1 "PWR_FLAG" H 5250 4123 50  0000 C CNN
F 2 "" H 5250 3950 50  0001 C CNN
F 3 "~" H 5250 3950 50  0001 C CNN
	1    5250 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	5250 3950 5000 3950
Connection ~ 5000 3950
$Comp
L Amplifier_Operational:LT1492 U401
U 1 1 60BC53A4
P 5100 3650
F 0 "U401" H 5100 4017 50  0000 C CNN
F 1 "LT1492" H 5100 3926 50  0000 C CNN
F 2 "lib:SOIC127P599X175-8N" H 5100 3650 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/14923f.pdf" H 5100 3650 50  0001 C CNN
	1    5100 3650
	1    0    0    -1  
$EndComp
$Comp
L Amplifier_Operational:LT1492 U402
U 1 1 60BC60DE
P 7050 3600
F 0 "U402" H 7050 3967 50  0000 C CNN
F 1 "LT1492" H 7050 3876 50  0000 C CNN
F 2 "lib:SOIC127P599X175-8N" H 7050 3600 50  0001 C CNN
F 3 "https://www.analog.com/media/en/technical-documentation/data-sheets/14923f.pdf" H 7050 3600 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
