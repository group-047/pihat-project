PCBNEW-LibModule-V1  2021-06-03 23:31:19
# encoding utf-8
Units mm
$INDEX
SMT-0603
$EndINDEX
$MODULE SMT-0603
Po 0 0 0 15 60b95837 00000000 ~~
Li SMT-0603
Cd SMT 0603
Kw LED
Sc 0
At SMD
AR 
Op 0 0 0
T0 -0.395 -0.96 1.27 1.27 0 0.254 N V 21 N "LED**"
T1 -0.395 -0.96 1.27 1.27 0 0.254 N I 21 N "SMT-0603"
DS -0.65 -0.4 0.65 -0.4 0.2 24
DS 0.65 -0.4 0.65 0.4 0.2 24
DS 0.65 0.4 -0.65 0.4 0.2 24
DS -0.65 0.4 -0.65 -0.4 0.2 24
DS -0.171 -0.4 0.171 -0.4 0.2 21
DS -0.171 0.4 0.171 0.4 0.2 21
DC -1.386 -0.043 -1.41659 -0.043 0.254 21
$PAD
Po -0.75 0
Sh "1" R 0.8 0.8 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$PAD
Po 0.75 0
Sh "2" R 0.8 0.8 0 0 900
At SMD N 00888000
Ne 0 ""
$EndPAD
$EndMODULE SMT-0603
$EndLIBRARY
