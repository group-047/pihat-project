EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Power Supply Unit"
Date "2021-06-04"
Rev "1"
Comp "Group 47"
Comment1 "Author: Nkosinathi Ntuli"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L OKI-78SR-5_1.5-W36-C:OKI-78SR-5_1.5-W36-C PS301
U 1 1 60BA71F7
P 5550 3950
F 0 "PS301" V 6238 3622 50  0000 R CNN
F 1 "OKI-78SR-5_1.5-W36-C" V 6147 3622 50  0000 R CNN
F 2 "lib:OKI78SR515W36C" H 6900 4050 50  0001 L CNN
F 3 "https://www.murata.com/products/productdata/8807037992990/oki-78sr.pdf?1583754815000" H 6900 3950 50  0001 L CNN
F 4 "Non-Isolated DC/DC Converters 7.5W 24Vin 5Vout1 1.5A SIP Non-Iso" H 6900 3850 50  0001 L CNN "Description"
F 5 "16.5" H 6900 3750 50  0001 L CNN "Height"
F 6 "Murata Electronics" H 6900 3650 50  0001 L CNN "Manufacturer_Name"
F 7 "OKI-78SR-5/1.5-W36-C" H 6900 3550 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "580-OKI78SR5/1.5W36C" H 6900 3450 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Murata-Power-Solutions/OKI-78SR-5-15-W36-C?qs=uJpRT2lXVNXJP%252Bo08dQqJQ%3D%3D" H 6900 3350 50  0001 L CNN "Mouser Price/Stock"
F 10 "OKI-78SR-5/1.5-W36-C" H 6900 3250 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/oki-78sr-51.5-w36-c/murata-power-solutions" H 6900 3150 50  0001 L CNN "Arrow Price/Stock"
	1    5550 3950
	0    -1   -1   0   
$EndComp
$Comp
L EEEFK1H221P:EEEFK1H221P C301
U 1 1 60BA94ED
P 6650 4150
F 0 "C301" V 6854 4280 50  0000 L CNN
F 1 "EEEFK1H221P" V 6945 4280 50  0000 L CNN
F 2 "lib:CAPAE1030X1080N" H 7000 4200 50  0001 L CNN
F 3 "https://industrial.panasonic.com/cdbs/www-data/pdf/RDE0000/ABA0000C1181.pdf" H 7000 4100 50  0001 L CNN
F 4 "Ecap 220uF 50V G case Panasonic Aluminium Electrolytic Capacitor 220uF 50 V dc 10mm Surface Mount series FK SMD lifetime 2000h" H 7000 4000 50  0001 L CNN "Description"
F 5 "10.8" H 7000 3900 50  0001 L CNN "Height"
F 6 "Panasonic" H 7000 3800 50  0001 L CNN "Manufacturer_Name"
F 7 "EEEFK1H221P" H 7000 3700 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 7000 3600 50  0001 L CNN "Mouser Part Number"
F 9 "" H 7000 3500 50  0001 L CNN "Mouser Price/Stock"
F 10 "EEEFK1H221P" H 7000 3400 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/eeefk1h221p/panasonic" H 7000 3300 50  0001 L CNN "Arrow Price/Stock"
	1    6650 4150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0303
U 1 1 60BAB5FF
P 6650 5100
F 0 "#PWR0303" H 6650 4850 50  0001 C CNN
F 1 "GND" H 6655 4927 50  0000 C CNN
F 2 "" H 6650 5100 50  0001 C CNN
F 3 "" H 6650 5100 50  0001 C CNN
	1    6650 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0302
U 1 1 60BABAB5
P 5650 5100
F 0 "#PWR0302" H 5650 4850 50  0001 C CNN
F 1 "GND" H 5655 4927 50  0000 C CNN
F 2 "" H 5650 5100 50  0001 C CNN
F 3 "" H 5650 5100 50  0001 C CNN
	1    5650 5100
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0301
U 1 1 60BAC079
P 4650 4200
F 0 "#PWR0301" H 4650 4050 50  0001 C CNN
F 1 "+12V" H 4665 4373 50  0000 C CNN
F 2 "" H 4650 4200 50  0001 C CNN
F 3 "" H 4650 4200 50  0001 C CNN
	1    4650 4200
	1    0    0    -1  
$EndComp
Text HLabel 7650 4150 2    50   Output ~ 0
5V
Wire Wire Line
	5550 3950 5550 4350
Wire Wire Line
	5550 4350 4650 4350
Wire Wire Line
	5650 3950 5650 5100
Wire Wire Line
	5750 3950 5750 4150
Wire Wire Line
	5750 4150 6650 4150
Wire Wire Line
	6650 4650 6650 5100
Wire Wire Line
	6650 4150 7650 4150
Connection ~ 6650 4150
$Comp
L power:PWR_FLAG #FLG0301
U 1 1 60C6F514
P 4250 4200
F 0 "#FLG0301" H 4250 4275 50  0001 C CNN
F 1 "PWR_FLAG" H 4250 4373 50  0000 C CNN
F 2 "" H 4250 4200 50  0001 C CNN
F 3 "~" H 4250 4200 50  0001 C CNN
	1    4250 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4200 4250 4350
Wire Wire Line
	4650 4350 4650 4200
Wire Wire Line
	4250 4350 4650 4350
Connection ~ 4650 4350
$EndSCHEMATC
