Info: Checking netlist symbol footprint "H101:/5C7C4C81:lib:MountingHole_2.7mm_M2.5_uHAT_RPi".
Info: Checking netlist symbol footprint "L101:/60BE11CE:RF_Module:Ai-Thinker-Ra-01-LoRa".
Info: Checking netlist symbol footprint "C301:/60B93462/60BA94ED:lib:CAPAE1030X1080N".
Info: Checking netlist symbol footprint "LED201:/60B93270/60BA20E6:lib:SMT-0603".
Info: Checking netlist symbol footprint "LED202:/60B93270/60BA26BD:lib:SMT-0603".
Info: Checking netlist symbol footprint "LED203:/60B93270/60BA3056:lib:SMT-0603".
Info: Checking netlist symbol footprint "LED204:/60B93270/60BA3402:lib:SMT-0603".
Info: Checking netlist symbol footprint "PS301:/60B93462/60BA71F7:lib:OKI78SR515W36C".
Info: Checking netlist symbol footprint "R201:/60B93270/60B9FE49:lib:RESC2012X60N".
Info: Checking netlist symbol footprint "R202:/60B93270/60BA0933:lib:RESC2012X60N".
Info: Checking netlist symbol footprint "R203:/60B93270/60BA13AD:lib:RESC2012X60N".
Info: Checking netlist symbol footprint "R204:/60B93270/60BA1700:lib:RESC2012X60N".
Info: Checking netlist symbol footprint "R402:/60B93622/60C53062:lib:RESC1005X40N".
Info: Checking netlist symbol footprint "H102:/5C7C7FBC:lib:MountingHole_2.7mm_M2.5_uHAT_RPi".
Info: Checking netlist symbol footprint "H103:/5C7C8014:lib:MountingHole_2.7mm_M2.5_uHAT_RPi".
Info: Checking netlist symbol footprint "H104:/5C7C8030:lib:MountingHole_2.7mm_M2.5_uHAT_RPi".
Info: Checking netlist symbol footprint "IC101:/60BBD9F4:lib:LP0003A".
Info: Checking netlist symbol footprint "J101:/5C77771F:lib:PinSocket_2x20_P2.54mm_Vertical_Centered_Anchor".
Info: Checking netlist symbol footprint "U402:/60B93622/60BA527E:lib:SOIC127P599X175-8N".
Info: Checking netlist symbol footprint "U401:/60B93622/60BA12C8:lib:SOIC127P599X175-8N".
Info: Checking netlist symbol footprint "R404:/60B93622/60C47F67:lib:RESC1005X40N".
Info: Checking netlist symbol footprint "R403:/60B93622/60C46ABB:lib:RESC1005X40N".
Info: Checking netlist symbol footprint "R401:/60B93622/60C4542C:lib:RESC1005X40N".
Changing footprint PS301 pad 1 net from  to +12V.
Changing footprint L101 pad 2 net from  to Net-(L101-Pad2).
Changing footprint L101 pad 4 net from  to Net-(L101-Pad4).
Changing footprint L101 pad 5 net from  to Net-(L101-Pad5).
Changing footprint L101 pad 6 net from  to Net-(L101-Pad6).
Changing footprint L101 pad 7 net from  to Net-(L101-Pad7).
Changing footprint L101 pad 8 net from  to Net-(L101-Pad8).
Changing footprint L101 pad 12 net from  to Net-(L101-Pad12).
Changing footprint J101 pad 40 net from  to /GPIO21.
Changing footprint L101 pad 13 net from  to Net-(L101-Pad13).
Changing footprint L101 pad 15 net from  to Net-(L101-Pad15).
Changing footprint J101 pad 3 net from  to /GPIO2_SDA1.
Changing footprint J101 pad 5 net from  to /GPIO3_SCL1.
Changing footprint J101 pad 7 net from  to /GPIO4_GPIO_GCLK.
Changing footprint J101 pad 11 net from  to /GPIO17_GEN0.
Changing footprint J101 pad 13 net from  to /GPIO27_GEN2.
Changing footprint J101 pad 15 net from  to /GPIO22_GEN3.
Changing footprint J101 pad 16 net from  to /GPIO23_GEN4.
Changing footprint J101 pad 18 net from  to /GPIO24_GEN5.
Changing footprint J101 pad 19 net from  to /GPIO10_SPI_MOSI.
Changing footprint J101 pad 21 net from  to /GPIO9_SPI_MISO.
Changing footprint J101 pad 22 net from  to /GPIO25_GEN6.
Changing footprint J101 pad 23 net from  to /GPIO11_SPI_SCLK.
Changing footprint J101 pad 24 net from  to /GPIO8_SPI_CE0_N.
Changing footprint J101 pad 26 net from  to /GPIO7_SPI_CE1_N.
Changing footprint J101 pad 27 net from  to /ID_SD.
Changing footprint J101 pad 28 net from  to /ID_SC.
Changing footprint J101 pad 29 net from  to /GPIO5.
Changing footprint J101 pad 31 net from  to /GPIO6.
Changing footprint J101 pad 32 net from  to /GPIO12.
Changing footprint J101 pad 33 net from  to /GPIO13.
Changing footprint J101 pad 35 net from  to /GPIO19.
Changing footprint J101 pad 36 net from  to /GPIO16.
Changing footprint J101 pad 38 net from  to /GPIO20.
Changing footprint L101 pad 14 net from  to Net-(L101-Pad14).
Error: Symbol L101 pad 17 not found in footprint RF_Module:Ai-Thinker-Ra-01-LoRa.

Error: Symbol L101 pad 18 not found in footprint RF_Module:Ai-Thinker-Ra-01-LoRa.

