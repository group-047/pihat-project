EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "uHAT Status LEDs "
Date "2021-06-03"
Rev "1"
Comp "Group 47"
Comment1 "Status LEDs Sub-module for the Raspberry Pi uHAT"
Comment2 "Author: Nkosinathi Ntuli"
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CR0805-JW-751ELF:CR0805-JW-751ELF R201
U 1 1 60B9FE49
P 4000 2500
F 0 "R201" V 4304 2588 50  0000 L CNN
F 1 "CR0805-JW-751ELF" V 4395 2588 50  0000 L CNN
F 2 "lib:RESC2012X60N" H 4550 2550 50  0001 L CNN
F 3 "https://www.bourns.com/pdfs/chpreztr.pdf" H 4550 2450 50  0001 L CNN
F 4 "Bourns CR0805 Series Thick Film Surface Mount Resistor 0805 Case 750 +/-5% 0.125W +/-200ppm/C" H 4550 2350 50  0001 L CNN "Description"
F 5 "0.6" H 4550 2250 50  0001 L CNN "Height"
F 6 "Bourns" H 4550 2150 50  0001 L CNN "Manufacturer_Name"
F 7 "CR0805-JW-751ELF" H 4550 2050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CR0805-JW-751ELF" H 4550 1950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=652-CR0805-JW-751ELF" H 4550 1850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CR0805-JW-751ELF" H 4550 1750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cr0805-jw-751elf/bourns" H 4550 1650 50  0001 L CNN "Arrow Price/Stock"
	1    4000 2500
	0    1    1    0   
$EndComp
$Comp
L CR0805-JW-751ELF:CR0805-JW-751ELF R202
U 1 1 60BA0933
P 5000 2500
F 0 "R202" V 5304 2588 50  0000 L CNN
F 1 "CR0805-JW-751ELF" V 5395 2588 50  0000 L CNN
F 2 "lib:RESC2012X60N" H 5550 2550 50  0001 L CNN
F 3 "https://www.bourns.com/pdfs/chpreztr.pdf" H 5550 2450 50  0001 L CNN
F 4 "Bourns CR0805 Series Thick Film Surface Mount Resistor 0805 Case 750 +/-5% 0.125W +/-200ppm/C" H 5550 2350 50  0001 L CNN "Description"
F 5 "0.6" H 5550 2250 50  0001 L CNN "Height"
F 6 "Bourns" H 5550 2150 50  0001 L CNN "Manufacturer_Name"
F 7 "CR0805-JW-751ELF" H 5550 2050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CR0805-JW-751ELF" H 5550 1950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=652-CR0805-JW-751ELF" H 5550 1850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CR0805-JW-751ELF" H 5550 1750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cr0805-jw-751elf/bourns" H 5550 1650 50  0001 L CNN "Arrow Price/Stock"
	1    5000 2500
	0    1    1    0   
$EndComp
$Comp
L CR0805-JW-751ELF:CR0805-JW-751ELF R203
U 1 1 60BA13AD
P 6000 2500
F 0 "R203" V 6304 2588 50  0000 L CNN
F 1 "CR0805-JW-751ELF" V 6395 2588 50  0000 L CNN
F 2 "lib:RESC2012X60N" H 6550 2550 50  0001 L CNN
F 3 "https://www.bourns.com/pdfs/chpreztr.pdf" H 6550 2450 50  0001 L CNN
F 4 "Bourns CR0805 Series Thick Film Surface Mount Resistor 0805 Case 750 +/-5% 0.125W +/-200ppm/C" H 6550 2350 50  0001 L CNN "Description"
F 5 "0.6" H 6550 2250 50  0001 L CNN "Height"
F 6 "Bourns" H 6550 2150 50  0001 L CNN "Manufacturer_Name"
F 7 "CR0805-JW-751ELF" H 6550 2050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CR0805-JW-751ELF" H 6550 1950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=652-CR0805-JW-751ELF" H 6550 1850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CR0805-JW-751ELF" H 6550 1750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cr0805-jw-751elf/bourns" H 6550 1650 50  0001 L CNN "Arrow Price/Stock"
	1    6000 2500
	0    1    1    0   
$EndComp
$Comp
L CR0805-JW-751ELF:CR0805-JW-751ELF R204
U 1 1 60BA1700
P 7000 2500
F 0 "R204" V 7304 2588 50  0000 L CNN
F 1 "CR0805-JW-751ELF" V 7395 2588 50  0000 L CNN
F 2 "lib:RESC2012X60N" H 7550 2550 50  0001 L CNN
F 3 "https://www.bourns.com/pdfs/chpreztr.pdf" H 7550 2450 50  0001 L CNN
F 4 "Bourns CR0805 Series Thick Film Surface Mount Resistor 0805 Case 750 +/-5% 0.125W +/-200ppm/C" H 7550 2350 50  0001 L CNN "Description"
F 5 "0.6" H 7550 2250 50  0001 L CNN "Height"
F 6 "Bourns" H 7550 2150 50  0001 L CNN "Manufacturer_Name"
F 7 "CR0805-JW-751ELF" H 7550 2050 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "652-CR0805-JW-751ELF" H 7550 1950 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=652-CR0805-JW-751ELF" H 7550 1850 50  0001 L CNN "Mouser Price/Stock"
F 10 "CR0805-JW-751ELF" H 7550 1750 50  0001 L CNN "Arrow Part Number"
F 11 "https://www.arrow.com/en/products/cr0805-jw-751elf/bourns" H 7550 1650 50  0001 L CNN "Arrow Price/Stock"
	1    7000 2500
	0    1    1    0   
$EndComp
$Comp
L LS_L29K-H1J2-1:LS_L29K-H1J2-1 LED201
U 1 1 60BA20E6
P 4000 4100
F 0 "LED201" V 4346 3970 50  0000 R CNN
F 1 "LS_L29K-H1J2-1" V 4255 3970 50  0000 R CNN
F 2 "lib:SMT-0603" H 4500 4250 50  0001 L BNN
F 3 "https://www.osram-os.com/Graphics/XPic2/00077099_0.pdf/LS%20L29K%20-%20SMARTLED%200603.pdf" H 4500 4150 50  0001 L BNN
F 4 "Hyper SMARTLED Super Red,LS L29K-H1J2-1 Osram Opto LS L29K-H1J2-1, SMARTLED Series Red High-Power LED, 630 nm 1608 (0603), Rectangle Lens SMD package" H 4500 4050 50  0001 L BNN "Description"
F 5 "" H 4500 3950 50  0001 L BNN "Height"
F 6 "OSRAM" H 4500 3850 50  0001 L BNN "Manufacturer_Name"
F 7 "LS L29K-H1J2-1" H 4500 3750 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "720-LSL29K-H1J2-1-Z" H 4500 3650 50  0001 L BNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=720-LSL29K-H1J2-1-Z" H 4500 3550 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 4500 3450 50  0001 L BNN "Arrow Part Number"
F 11 "" H 4500 3350 50  0001 L BNN "Arrow Price/Stock"
	1    4000 4100
	0    -1   -1   0   
$EndComp
$Comp
L LS_L29K-H1J2-1:LS_L29K-H1J2-1 LED202
U 1 1 60BA26BD
P 5000 4100
F 0 "LED202" V 5346 3970 50  0000 R CNN
F 1 "LS_L29K-H1J2-1" V 5255 3970 50  0000 R CNN
F 2 "lib:SMT-0603" H 5500 4250 50  0001 L BNN
F 3 "https://www.osram-os.com/Graphics/XPic2/00077099_0.pdf/LS%20L29K%20-%20SMARTLED%200603.pdf" H 5500 4150 50  0001 L BNN
F 4 "Hyper SMARTLED Super Red,LS L29K-H1J2-1 Osram Opto LS L29K-H1J2-1, SMARTLED Series Red High-Power LED, 630 nm 1608 (0603), Rectangle Lens SMD package" H 5500 4050 50  0001 L BNN "Description"
F 5 "" H 5500 3950 50  0001 L BNN "Height"
F 6 "OSRAM" H 5500 3850 50  0001 L BNN "Manufacturer_Name"
F 7 "LS L29K-H1J2-1" H 5500 3750 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "720-LSL29K-H1J2-1-Z" H 5500 3650 50  0001 L BNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=720-LSL29K-H1J2-1-Z" H 5500 3550 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 5500 3450 50  0001 L BNN "Arrow Part Number"
F 11 "" H 5500 3350 50  0001 L BNN "Arrow Price/Stock"
	1    5000 4100
	0    -1   -1   0   
$EndComp
$Comp
L LS_L29K-H1J2-1:LS_L29K-H1J2-1 LED203
U 1 1 60BA3056
P 6000 4100
F 0 "LED203" V 6346 4330 50  0000 L CNN
F 1 "LS_L29K-H1J2-1" V 6255 4330 50  0000 L CNN
F 2 "lib:SMT-0603" H 6500 4250 50  0001 L BNN
F 3 "https://www.osram-os.com/Graphics/XPic2/00077099_0.pdf/LS%20L29K%20-%20SMARTLED%200603.pdf" H 6500 4150 50  0001 L BNN
F 4 "Hyper SMARTLED Super Red,LS L29K-H1J2-1 Osram Opto LS L29K-H1J2-1, SMARTLED Series Red High-Power LED, 630 nm 1608 (0603), Rectangle Lens SMD package" H 6500 4050 50  0001 L BNN "Description"
F 5 "" H 6500 3950 50  0001 L BNN "Height"
F 6 "OSRAM" H 6500 3850 50  0001 L BNN "Manufacturer_Name"
F 7 "LS L29K-H1J2-1" H 6500 3750 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "720-LSL29K-H1J2-1-Z" H 6500 3650 50  0001 L BNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=720-LSL29K-H1J2-1-Z" H 6500 3550 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 6500 3450 50  0001 L BNN "Arrow Part Number"
F 11 "" H 6500 3350 50  0001 L BNN "Arrow Price/Stock"
	1    6000 4100
	0    1    -1   0   
$EndComp
$Comp
L LS_L29K-H1J2-1:LS_L29K-H1J2-1 LED204
U 1 1 60BA3402
P 7000 4100
F 0 "LED204" V 7346 4330 50  0000 L CNN
F 1 "LS_L29K-H1J2-1" V 7255 4330 50  0000 L CNN
F 2 "lib:SMT-0603" H 7500 4250 50  0001 L BNN
F 3 "https://www.osram-os.com/Graphics/XPic2/00077099_0.pdf/LS%20L29K%20-%20SMARTLED%200603.pdf" H 7500 4150 50  0001 L BNN
F 4 "Hyper SMARTLED Super Red,LS L29K-H1J2-1 Osram Opto LS L29K-H1J2-1, SMARTLED Series Red High-Power LED, 630 nm 1608 (0603), Rectangle Lens SMD package" H 7500 4050 50  0001 L BNN "Description"
F 5 "" H 7500 3950 50  0001 L BNN "Height"
F 6 "OSRAM" H 7500 3850 50  0001 L BNN "Manufacturer_Name"
F 7 "LS L29K-H1J2-1" H 7500 3750 50  0001 L BNN "Manufacturer_Part_Number"
F 8 "720-LSL29K-H1J2-1-Z" H 7500 3650 50  0001 L BNN "Mouser Part Number"
F 9 "https://www.mouser.com/Search/Refine.aspx?Keyword=720-LSL29K-H1J2-1-Z" H 7500 3550 50  0001 L BNN "Mouser Price/Stock"
F 10 "" H 7500 3450 50  0001 L BNN "Arrow Part Number"
F 11 "" H 7500 3350 50  0001 L BNN "Arrow Price/Stock"
	1    7000 4100
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR0202
U 1 1 60BA3C63
P 4000 5000
F 0 "#PWR0202" H 4000 4750 50  0001 C CNN
F 1 "GND" H 4005 4827 50  0000 C CNN
F 2 "" H 4000 5000 50  0001 C CNN
F 3 "" H 4000 5000 50  0001 C CNN
	1    4000 5000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0201
U 1 1 60BA4999
P 4000 2000
F 0 "#PWR0201" H 4000 1850 50  0001 C CNN
F 1 "+3.3V" H 4015 2173 50  0000 C CNN
F 2 "" H 4000 2000 50  0001 C CNN
F 3 "" H 4000 2000 50  0001 C CNN
	1    4000 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0203
U 1 1 60BA51E3
P 5000 2000
F 0 "#PWR0203" H 5000 1850 50  0001 C CNN
F 1 "+3.3V" H 5015 2173 50  0000 C CNN
F 2 "" H 5000 2000 50  0001 C CNN
F 3 "" H 5000 2000 50  0001 C CNN
	1    5000 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0204
U 1 1 60BA56CF
P 6000 2000
F 0 "#PWR0204" H 6000 1850 50  0001 C CNN
F 1 "+3.3V" H 6015 2173 50  0000 C CNN
F 2 "" H 6000 2000 50  0001 C CNN
F 3 "" H 6000 2000 50  0001 C CNN
	1    6000 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0205
U 1 1 60BA58BB
P 7000 2000
F 0 "#PWR0205" H 7000 1850 50  0001 C CNN
F 1 "+3.3V" H 7015 2173 50  0000 C CNN
F 2 "" H 7000 2000 50  0001 C CNN
F 3 "" H 7000 2000 50  0001 C CNN
	1    7000 2000
	1    0    0    -1  
$EndComp
Text HLabel 6000 5000 3    50   Input ~ 0
Tx
Text HLabel 7000 5000 3    50   Input ~ 0
Rx
Text HLabel 5000 5000 3    50   Input ~ 0
AUX
Wire Wire Line
	4000 2000 4000 2500
Wire Wire Line
	4000 3200 4000 3500
Wire Wire Line
	4000 4100 4000 5000
Wire Wire Line
	5000 2000 5000 2500
Wire Wire Line
	5000 3200 5000 3500
Wire Wire Line
	5000 4100 5000 5000
Wire Wire Line
	6000 2000 6000 2500
Wire Wire Line
	7000 3200 7000 3500
Wire Wire Line
	7000 2500 7000 2000
Wire Wire Line
	7000 4100 7000 5000
Wire Wire Line
	6000 3200 6000 3500
Wire Wire Line
	6000 4100 6000 5000
$EndSCHEMATC
